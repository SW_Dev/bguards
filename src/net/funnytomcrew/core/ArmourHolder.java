/*
 * Copyright (c) 2015 SW_Dev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 *  to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.funnytomcrew.core;

import org.bukkit.inventory.ItemStack;

/**
 * Created by Sean on 23/04/2015.
 */
public class ArmourHolder {

    ItemStack helmet;
    ItemStack chesplate;
    ItemStack leggings;
    ItemStack boots;

    public ArmourHolder(ItemStack helmet, ItemStack boots, ItemStack leggings, ItemStack chesplate) {
        this.helmet = helmet;
        this.boots = boots;
        this.leggings = leggings;
        this.chesplate = chesplate;
    }

    public ItemStack[] getAsArray() {
        ItemStack[] is = {helmet, chesplate, leggings, boots};
        return is;
    }

    @Override
    public String toString() {
        return "ArmourHolder{" +
                "helmet=" + helmet +
                ", chesplate=" + chesplate +
                ", leggings=" + leggings +
                ", boots=" + boots +
                '}';
    }

    public ItemStack getHelmet() {
        return helmet;
    }

    public void setHelmet(ItemStack helmet) {
        this.helmet = helmet;
    }

    public ItemStack getChesplate() {
        return chesplate;
    }

    public void setChesplate(ItemStack chesplate) {
        this.chesplate = chesplate;
    }

    public ItemStack getLeggings() {
        return leggings;
    }

    public void setLeggings(ItemStack leggings) {
        this.leggings = leggings;
    }

    public ItemStack getBoots() {
        return boots;
    }

    public void setBoots(ItemStack boots) {
        this.boots = boots;
    }
}

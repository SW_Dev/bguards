/*
 * Copyright (c) 2015 SW_Dev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 *  to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.funnytomcrew.core;

import net.funnytomcrew.core.enviousapi.config.Config;
import net.funnytomcrew.core.util.CommandUtils;
import net.funnytomcrew.core.util.MessageUtil;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;

/**
 * Created by Sean on 20/04/2015.
 */

public class FtcGuards extends JavaPlugin implements Listener {

    Config Messages;
    Config Config;
    Config duty;
    MessageUtil util;
    CommandUtils cutil;

    ArrayList<UUID> onDuty = new ArrayList<>();
    List<UUID> cooldown = new ArrayList<>();
    HashMap<UUID, IHolder> dutyInvs = new HashMap<>();

    @Override
    public void onEnable() {
        Messages = new Config(this, "messages");
        Config = new Config(this, "itemc");
        util = new MessageUtil(this);
        cutil = new CommandUtils(this);
        duty = new Config(this, "duty");

        Bukkit.getPluginManager().registerEvents(this, this);

        List<String> UUIDLIST = duty.getStringList("uuid");
        for (String string : UUIDLIST) {
            UUID uuid = UUID.fromString(string);
            onDuty.add(uuid);
        }

        util.load();

    }

    public void onDisable() {
        List<String> UUIDLIST = new ArrayList<>();

        for (UUID uuid : onDuty) {
            UUIDLIST.add(uuid.toString());
        }

        duty.set("uuid", UUIDLIST);
        duty.save();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        //Checks if the command sender is a player
        if (sender instanceof Player) {

            final Player player = (Player) sender; //Get a player instance so we can get the UUID

            if (label.equalsIgnoreCase("duty") && sender.hasPermission("ftcguards.duty") && !cooldown.contains(player.getUniqueId())) {
                if (onDuty.contains(player.getUniqueId())) {
                    String tag = Messages.getString("tag");
                    String message = Messages.getString("off-duty");
                    player.getInventory().clear();
                    util.broadcast(tag, message, player);
                    cooldown.add(player.getUniqueId());

                    new BukkitRunnable() {
                        @Override
                        public void run() {
                            IHolder ih = dutyInvs.get(player.getUniqueId());
                            ArmourHolder ah = ih.getArmourHolder();
                            ItemStack[] contents = ih.getInventoryContents();

                            if (contents != null) {
                                player.getInventory().setContents(contents);
                            } else {
                                player.getInventory().clear();
                            }

                            if (ah.getAsArray() != null) {
                                player.getInventory().setHelmet(ah.getHelmet());
                                player.getInventory().setChestplate(ah.getBoots());
                                player.getInventory().setLeggings(ah.getLeggings());
                                player.getInventory().setBoots(ah.getChesplate());
                            } else {
                                player.getInventory().setArmorContents(null);
                            }

                            player.updateInventory();

                            onDuty.remove(player.getUniqueId()); // Set player off duty
                            cooldown.remove(player.getUniqueId());
                            dutyInvs.remove(player.getUniqueId());
                        }
                    }.runTaskLater(this, 20);
                } else {
                    ArmourHolder ah = new ArmourHolder(player.getInventory().getHelmet(), player.getInventory().getChestplate(), player.getInventory().getLeggings(), player.getInventory().getBoots());
                    IHolder ih = new IHolder(player.getInventory().getContents(), ah);
                    dutyInvs.put(player.getUniqueId(), ih);
                    onDuty.add(player.getUniqueId()); // Set player on duty
                    player.getInventory().clear();
                    player.getInventory().setArmorContents(null);
                    player.updateInventory();
                    String tag = Messages.getString("tag");
                    String message = Messages.getString("on-duty");
                    util.broadcast(tag, message, player);
                    util.setGuard(player);
                }
            }

            //Countdown commands (See CommandUtils)
            if (label.equalsIgnoreCase("sword") && args.length == 1 && player.hasPermission("ftcguards.sword")) {
                cutil.countdown(player, Bukkit.getPlayer(args[0]), 5, CommandUtils.WeaponType.SWORD);
            }

            if (label.equalsIgnoreCase("bow") && args.length == 1 && player.hasPermission("ftcguards.bow")) {
                cutil.countdown(player, Bukkit.getPlayer(args[0]), 5, CommandUtils.WeaponType.BOW);
            }

            if (label.equalsIgnoreCase("axe") && args.length == 1 && player.hasPermission("ftcguards.axe")) {
                cutil.countdown(player, Bukkit.getPlayer(args[0]), 5, CommandUtils.WeaponType.AXE);
            }

            //Lists online guards
            if (label.equalsIgnoreCase("guards") && player.hasPermission("ftcguards.guards")) {
                List<String> GuardsOnDuty = new ArrayList<>();
                List<String> GuardsOffDuty = new ArrayList<>();
                for (Player p : Bukkit.getOnlinePlayers()) {
                    if (p.hasPermission("ftcguards.duty")) {
                        if (onDuty.contains(p.getUniqueId())) {
                            GuardsOnDuty.add(p.getName());
                        } else {
                            GuardsOffDuty.add(p.getName());
                        }
                    }
                }

                String od = Messages.getString("guards-onduty");
                String ofd = Messages.getString("guards-offduty");
                String head = ChatColor.translateAlternateColorCodes('&', Messages.getString("guards-header"));

                String onDuty = od + GuardsOnDuty.toString();
                onDuty = onDuty.replace("[", "").replace("]", "");
                String offDuty = ofd + GuardsOffDuty.toString();
                offDuty = offDuty.replace("[", "").replace("]", "");
                onDuty = ChatColor.translateAlternateColorCodes('&', onDuty);
                offDuty = ChatColor.translateAlternateColorCodes('&', offDuty);
                sender.sendMessage(head);
                sender.sendMessage(onDuty);
                sender.sendMessage(offDuty);
                sender.sendMessage(head);
            }

            if (label.equalsIgnoreCase("ftcgabout")) {
                sender.sendMessage(ChatColor.translateAlternateColorCodes('&', Messages.getString("about-header")));
                sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&aVersion: 2.0"));
                sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&aAuthor: Yahweh for FTC Network"));
            }

            if (label.equalsIgnoreCase("greload") && player.isOp()) {
                util.reload();
                sender.sendMessage(ChatColor.translateAlternateColorCodes('&', Messages.getShortList("TAG") + Messages.getString("message-seperator") + Messages.getString("reload")));
            }

        }


        return false;
    }

    @EventHandler
    public void isOnDuty(PlayerDropItemEvent event) {
        if (onDuty.contains(event.getPlayer().getUniqueId())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onGuardDeath(PlayerDeathEvent event) {
        if (onDuty.contains(event.getEntity().getUniqueId())) {
            event.setKeepInventory(true);
        }
    }

    @EventHandler
    public void onChat(AsyncPlayerChatEvent event) {
        if (onDuty.contains(event.getPlayer().getUniqueId()) || event.getPlayer().isOp() == true) {
            if (event.getMessage().startsWith("!")) {
                String message = event.getMessage();
                message = ChatColor.translateAlternateColorCodes('&', message);
                message = message.replaceFirst("!", "");
                event.setCancelled(true);

                if (event.getPlayer().isOp() || event.getPlayer().hasPermission("ftcguards.gchat")) {
                    event.getPlayer().sendMessage(formatGuardMessage(message, event.getPlayer()));
                }

                Bukkit.getLogger().log(Level.INFO, formatGuardMessage(message, event.getPlayer()));

                for (UUID uuid : onDuty) {
                    if (Bukkit.getPlayer(uuid).isOp() == false || Bukkit.getPlayer(uuid).hasPermission("ftcguards.gchat") == false) {
                        Bukkit.getPlayer(uuid).sendMessage(formatGuardMessage(message, event.getPlayer()));

                    }
                }
            }
        }
    }

    public String formatGuardMessage(String msg, Player player) {
        String fm = "";
        Config mssg = new Config(this, "messages");
        String prefix = "[ERROR]";

        if (player.hasPermission("ftcguards.helper")) {
            prefix = mssg.getString("staff-guard-prefix");
        }

        if (player.hasPermission("ftcguards.mod")) {
            prefix = mssg.getString("staff-mod-prefix");
        }

        if (player.hasPermission("ftcguards.admin")) {
            prefix = mssg.getString("staff-admin-prefix");
        }

        if (player.hasPermission("ftcguards.op") || player.isOp() == true) {
            prefix = mssg.getString("staff-op-prefix");
        }

        if (player.getName() == "Yahweh") {
            prefix = mssg.getString("staff-dev-prefix");
        }

        if (player.getName() == "Funnytom118") {
            prefix = mssg.getString("staff-owner-prefix");
        }

        fm = fm + prefix;

        fm = fm + mssg.getString("staff-formating-colour");
        fm = fm + player.getName();
        fm = fm + mssg.getString("staff-seperator") + msg;

        fm = ChatColor.translateAlternateColorCodes('&', fm);

        return fm;
    }
}

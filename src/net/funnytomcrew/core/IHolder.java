/*
 * Copyright (c) 2015 SW_Dev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 *  to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.funnytomcrew.core;

import org.bukkit.inventory.ItemStack;

import java.util.Arrays;

/**
 * Created by Sean on 23/04/2015.
 */
public class IHolder {

    ItemStack[] inventoryContents;
    ArmourHolder armourHolder;

    public IHolder(ItemStack[] inventoryContents, ArmourHolder armourHolder) {
        this.inventoryContents = inventoryContents;
        this.armourHolder = armourHolder;
    }

    public ItemStack[] getInventoryContents() {
        return inventoryContents;
    }

    public ArmourHolder getArmourHolder() {
        return armourHolder;
    }

    @Override
    public String toString() {
        return "IHolder{" +
                "inventoryContents=" + Arrays.toString(inventoryContents) +
                ", armourHolder=" + armourHolder +
                '}';
    }
}

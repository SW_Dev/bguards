/*
 * Copyright (c) 2015 SW_Dev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 *  to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.funnytomcrew.core.util;

import net.funnytomcrew.core.FtcGuards;
import net.funnytomcrew.core.enviousapi.config.Config;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * Created by Sean on 20/04/2015.
 */
public class CommandUtils {

    FtcGuards plugin;

    public CommandUtils(FtcGuards plugin) {
        this.plugin = plugin;
    }

    /**
     * @param reciver - The player that is being counted down
     * @param sender  - The guard who issued the command
     * @param type    - The weapon type
     * @param time    - The count down time
     */
    public void countdown(final Player sender, final Player reciver, int time, final WeaponType type) {
        final Config msg = new Config(plugin, "messages");
        int b = time;
        for (int i = 1; i <= time + 1; i++) {
            if (i != 1) {
                b--;
            }
            final int a = b;
            new BukkitRunnable() {
                @Override
                public void run() {
                    String tag = ChatColor.translateAlternateColorCodes('&', msg.getString("tag"));
                    String sep = ChatColor.translateAlternateColorCodes('&', msg.getString("message-seperator"));
                    String mesg = "DEFAULT SOMETHING WENT WRONG";
                    String gmsg = "DEFAULT SOMETHING WENT WRONG";

                    if (type == WeaponType.SWORD) {
                        mesg = ChatColor.translateAlternateColorCodes('&', msg.getString("countdown-sword"));
                        gmsg = ChatColor.translateAlternateColorCodes('&', msg.getString("guard-countdown-sword"));
                    }
                    if (type == WeaponType.AXE) {
                        mesg = ChatColor.translateAlternateColorCodes('&', msg.getString("countdown-axe"));
                        gmsg = ChatColor.translateAlternateColorCodes('&', msg.getString("guard-countdown-axe"));
                    }
                    if (type == WeaponType.BOW) {
                        mesg = ChatColor.translateAlternateColorCodes('&', msg.getString("countdown-bow"));
                        gmsg = ChatColor.translateAlternateColorCodes('&', msg.getString("guard-countdown-bow"));
                    }
                    if (mesg.contains("TIME")) {
                        mesg = mesg.replaceAll("TIME", Integer.toString(a));
                        gmsg = gmsg.replaceAll("TIME", Integer.toString(a));
                    }
                    if (mesg.contains("PLAYER")) {
                        mesg = mesg.replaceAll("PLAYER", sender.getName());
                        gmsg = gmsg.replaceAll("PLAYER", sender.getName());
                    }

                    sender.sendMessage(tag + sep + " " + gmsg);
                    reciver.sendMessage(tag + sep + " " + mesg);
                }
            }.runTaskLater(plugin, 20 * i);
        }
    }

    /**
     * AXE - Axe weapon
     * BOW - Bow weapon
     * SWORD - Sword Weapon
     */
    public enum WeaponType {
        AXE,
        BOW,
        SWORD
    }
}

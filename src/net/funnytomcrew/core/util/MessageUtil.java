/*
 * Copyright (c) 2015 SW_Dev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 *  to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.funnytomcrew.core.util;

import net.funnytomcrew.core.FtcGuards;
import net.funnytomcrew.core.enviousapi.config.Config;
import net.funnytomcrew.core.enviousapi.itemstackbuilder.ItemStackBuilder;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sean on 20/04/2015.
 */
public class MessageUtil {

    FtcGuards plugin;
    ArrayList<ItemStack> items = new ArrayList<>(); // ItemStack list

    public MessageUtil(FtcGuards plugin) {
        this.plugin = plugin;
    }

    /**
     * @param tag     - The message prefix
     * @param Message - The message itself
     * @param guard   - The command issuer
     */
    public void broadcast(String tag, String Message, Player guard) {
        if (Message.contains("PLAYER")) {
            Message = Message.replace("PLAYER", guard.getName());
        }

        Config cfg = new Config(plugin, "messages");

        tag = ChatColor.translateAlternateColorCodes('&', tag);
        Message = ChatColor.translateAlternateColorCodes('&', Message);
        Bukkit.broadcastMessage(tag + cfg.getString("message-seperator") + " " + Message);
    }

    /**
     * @param player - The guard
     */
    public void setGuard(Player player) {

        for (ItemStack is : items) { // gives items to guard
            assert is != null;
            assert player != null;
            player.getInventory().addItem(is);
        }

    }

    public void reload() {
        items.clear();
        load();
    }

    public void load() {
        Config config = new Config(plugin, "itemc");

        ConfigurationSection cs = config.getConfigurationSection("items"); // Get the configuration section for items

        for (String string : cs.getKeys(false)) { // Load in the items from config
            Material type = Material.getMaterial(cs.getString(string + ".material"));
            String name = cs.getString(string + ".name");
            int data = cs.getInt(string + ".data");
            int amount = cs.getInt(string + ".amount");
            List<String> enchants = cs.getStringList(string + ".enchants");
            List<String> lore = cs.getStringList(string + ".lore");

            if (type == null) {
                System.out.println("Material type is null for " + string);
            }

            ItemStackBuilder builder = new ItemStackBuilder(type);

            if (type == Material.GOLDEN_APPLE) {
                if (data == 1) {
                    builder.setDurability((short) 1);
                }
            }

            //builder.setData(data);
            builder.setAmount(amount);

            for (String str : enchants) {
                String array[] = str.split("/");
                builder.enchant(Enchantment.getByName(array[0]), Integer.valueOf(array[1]));
            }


            for (String st : lore) {
                builder.addLore(ChatColor.translateAlternateColorCodes('&', st));
            }

            name = ChatColor.translateAlternateColorCodes('&', name);
            builder.setName(name);

            items.add(builder.get());
        }
    }


}
